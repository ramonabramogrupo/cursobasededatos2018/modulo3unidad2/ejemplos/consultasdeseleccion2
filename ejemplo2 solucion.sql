﻿/* CONSULTAS DE TOTALES */
-- 1	Numero de ciclistas que hay

SELECT COUNT(*) AS nciclistas FROM ciclista c;

-- 2	Numero de ciclistas que hay del equipo Banesto
SELECT 
  COUNT(*)
FROM 
  ciclista c
WHERE
  c.nomequipo='Banesto';

-- 3	La edad media de los ciclistas

  SELECT 
    AVG(edad) AS edadMedia 
  FROM 
    ciclista c;

-- 4	La edad media de los de equipo Banesto

  SELECT 
    AVG(edad) AS edadMedia 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto';

-- 5	La edad media de los ciclistas por cada equipo

  SELECT 
    c.nomequipo, AVG(edad) AS edadMedia 
  FROM 
    ciclista c 
  GROUP BY 
    c.nomequipo;


-- 6	El numero de ciclistas por equipo	
  
  SELECT 
    COUNT(*),c.nomequipo 
  FROM 
    ciclista c 
  GROUP BY 
    c.nomequipo;


-- 7	El número total de puertos

  SELECT 
    COUNT(*) AS nPuertos 
  FROM 
    puerto p;

-- 8	El número total de puertos mayores de 1500

  SELECT 
    COUNT(*) AS nPuertos 
  FROM 
    puerto p 
  WHERE 
    p.altura>1500;

-- 9	Listar el nombre de los equipos que tengan más de 4 ciclistas
  
  SELECT 
    COUNT(*) AS nCiclistas,
    c.nomequipo
  FROM 
    ciclista c 
  GROUP BY 
    c.nomequipo
  HAVING
    nCiclistas>4;


  -- lo mismo que la anterior pero sin mostrar el numero de ciclistas
  SELECT 
    c.nomequipo
  FROM 
    ciclista c 
  GROUP BY 
    c.nomequipo
  HAVING
    COUNT(*)>4;

  -- utilizar subconsultas
  -- c1 el numero de ciclistas por equipo
  SELECT 
    COUNT(*) AS nCiclistas,
    c.nomequipo
  FROM 
    ciclista c 
  GROUP BY 
    c.nomequipo;
  -- 
    SELECT 
      c1.nomequipo 
    FROM 
      (SELECT 
          COUNT(*) AS nCiclistas,
          c.nomequipo
        FROM 
          ciclista c 
        GROUP BY 
          c.nomequipo) AS c1
    WHERE
      c1.nCiclistas>4;


-- 10	Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32

  SELECT 
    c.nomequipo 
  FROM 
    ciclista c 
  WHERE 
    c.edad BETWEEN 28 AND 32
  GROUP BY
    c.nomequipo
  HAVING 
    COUNT(*)>4;

-- 11	Indícame el número de etapas que ha ganado cada uno de los ciclistas
  
  SELECT 
    dorsal,COUNT(*) AS nEtapas 
  FROM 
    etapa e 
  GROUP BY 
    e.dorsal;


-- 12	Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa	
  
  SELECT 
    e.dorsal 
  FROM 
    etapa e 
  GROUP BY 
    dorsal 
  HAVING 
    COUNT(*)>1;



